import firebase from "firebase/app";
import "firebase/firestore";

// Prevent SSR of Nuxt.
if (!firebase.apps.length) {
  var config = {
    apiKey: "AIzaSyCVbiLpkH3JZOznKAZL5Gebgn_BExROi_Q",
    authDomain: "nuxt-firestore-news-feed.firebaseapp.com",
    databaseURL: "https://nuxt-firestore-news-feed.firebaseio.com",
    projectId: "nuxt-firestore-news-feed",
    storageBucket: "nuxt-firestore-news-feed.appspot.com",
    messagingSenderId: "270700135572"
  };
  firebase.initializeApp(config);
  firebase.firestore().settings({
    timestampsInSnapshots: true
  });
}

const db = firebase.firestore();

export default db;
